package com.androidgame.motorlite;

import java.util.Random;


public class AIMotor extends Motor {
	Random rand = new Random();
	public AIMotor(float x, float y, int type) {
		super(x, y, type);
	}
	
	public void lift() {
		if (rand.nextFloat() > 0.9f) {
			super.lift();
		}
	}
	
	public void evade(int direct) {
		if (rand.nextFloat() <= 0.6f) {
			super.changeLane(direct);
		}
	}
	
	public void hinder(Motor player) {
		float gapX = position.x - player.position.x;
		float gapY = position.y - player.position.y;
		if (gapX >= 0 && gapX <= 5 && gapY != 0 && gapY != 0.4f && gapY != -0.4f) {
			float chance = gapX == 0 ? 0.9f : 0.5f;
			if (rand.nextFloat() >= chance) {
				if (position.y >= 4.6f) {
					position.y -= 1.8f;
				} else {
					position.y += 1.8f;
				}
			}
		}
	}
}
