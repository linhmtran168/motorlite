package com.androidgame.motorlite;

import com.androidgame.framework.Music;
import com.androidgame.framework.Sound;
import com.androidgame.framework.gl.Animation;
import com.androidgame.framework.gl.Font;
import com.androidgame.framework.gl.Texture;
import com.androidgame.framework.gl.TextureRegion;
import com.androidgame.framework.ngin.GLGame;

public class Assets {
    public static Texture[] maps;
    public static TextureRegion mapRegion;
    
    public static Texture items;        
    public static TextureRegion nails;
    public static TextureRegion can;
    public static TextureRegion brick;
    public static TextureRegion milestone;
    public static TextureRegion flag;
    public static TextureRegion minimap;
    public static TextureRegion mapPlayer;
    public static TextureRegion mapAI;
    
    public static Texture menus;
    public static TextureRegion pausedMenu;
    public static TextureRegion gameOverMenu;
    public static TextureRegion rightMenu;
    public static TextureRegion leftMenu;
    public static TextureRegion[] readyMenu;
    public static Texture motors;
    public static Animation motor;
    public static Animation aiMotor;
    
    public static Texture fontTex;
    public static Texture speedFontTex;
    
    public static Music music;
    public static Sound sound;
    
    public static Font font;
    public static Font speedFont;
    
    public static void load(GLGame game) {
    	maps  = new Texture[4];
    	for (int i = 0; i < 4; i++) {
	        maps[i] = new Texture(game, "map" + i +".png");;
    	}
        
        items = new Texture(game, "items.png");        
        brick = new TextureRegion(items, 0, 0, 64, 64);
        can = new TextureRegion(items, 64, 0, 64, 64);
        nails = new TextureRegion(items, 128, 0, 64, 32);
        flag = new TextureRegion(items, 0, 64, 416, 416);
        milestone = new TextureRegion(items, 192, 0, 64, 64);
        minimap = new TextureRegion(items, 256, 0, 640, 44);
        mapPlayer = new TextureRegion(items, 896, 0, 10, 10);
        mapAI = new TextureRegion(items, 928, 0, 10, 10);
        
        menus = new Texture(game, "menu.png");
        pausedMenu = new TextureRegion(menus, 0, 0, 144, 288);
        gameOverMenu = new TextureRegion(menus, 160, 0, 144, 288);
        rightMenu = new TextureRegion(menus, 320, 0, 64, 192);
        leftMenu = new TextureRegion(menus, 384, 0, 64, 192);
        
        
        readyMenu = new TextureRegion[3];
        for (int i = 0; i < 3; i++) {
        	readyMenu[i] = new TextureRegion(menus, 160*i, 288, 144, 224);
        }
        
        motors = new Texture(game, "motors.png");
        motor = new Animation(0.01f,
        		new TextureRegion(motors, 0, 0, 160, 160),
        		new TextureRegion(motors, 160, 0, 160, 160)
        );
        aiMotor = new Animation(0.01f,
        		new TextureRegion(motors, 0, 160, 160, 160),
        		new TextureRegion(motors, 160, 160, 160, 160)
       ); 
        
        speedFontTex = new Texture(game, "speedfont.png");
        fontTex = new Texture(game, "font.png");
        font = new Font(fontTex, 0, 0, 12, 21, 28);
        speedFont = new Font(speedFontTex, 0, 0, 8, 32, 32);
//        for (int i = 0; i < 2; i++) {
//			motor[i] = new TextureRegion(motors, 0, 0+i*160, 160, 160);
//		}
        
    }       
    
    public static Texture geneText(GLGame game, String text, int size) {
    	return new Texture(game, text, size);
    }
    
    public static void reload() {
    	for (int i = 0; i < 4; i++) {
    		maps[i].reload();
    	}
        items.reload();
        if(Settings.soundEnabled)
            music.play();
    }
    
    public static void playSound(Sound sound) {
        if(Settings.soundEnabled)
            sound.play(1);
    }
}
