package com.androidgame.motorlite;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.androidgame.framework.Game;
import com.androidgame.framework.Input.TouchEvent;
import com.androidgame.framework.gl.Camera2D;
import com.androidgame.framework.gl.FPSCounter;
import com.androidgame.framework.gl.SpriteBatcher;
import com.androidgame.framework.gl.Texture;
import com.androidgame.framework.gl.TextureRegion;
import com.androidgame.framework.math.OverlapTester;
import com.androidgame.framework.math.Rectangle;
import com.androidgame.framework.math.Vector2;
import com.androidgame.framework.ngin.GLGame;
import com.androidgame.framework.ngin.GLScreen;

public class GameScreen extends GLScreen {
	enum GameState {
		Ready1,
		Ready2,
		Ready3,
		Running,
		Paused,
		GameOver
	}
	
	GameState state;
	Camera2D cam2d;
	SpriteBatcher batcher;
	Vector2 touchPoint;
	Rectangle replay1Bounds;
	Rectangle replay2Bounds;
	Rectangle resumeBounds; 
	Rectangle pauseBounds;
	Rectangle upBounds;
	Rectangle downBounds;
	Rectangle liftBounds;
	WorldRenderer renderer;
	World world;
	FPSCounter fpsCounter;
	float startTime;
	Texture overText;
	
	public GameScreen(Game game) {
		super(game);
		state = GameState.Ready1;
		cam2d = new Camera2D(glGraphics, 800, 480);
		touchPoint = new Vector2();
		batcher = new SpriteBatcher(glGraphics, 1000);
		pauseBounds = new Rectangle(0, 288, 64, 64);
		liftBounds = new Rectangle(0, 160, 64, 64);
		upBounds = new Rectangle(800-64, 288, 64, 64);
		downBounds = new Rectangle(800-64, 160, 64, 64);
		resumeBounds = new Rectangle(352, 256, 120, 40);
		replay1Bounds = new Rectangle(352, 192, 120, 40);
		replay2Bounds = new Rectangle(352, 216, 120, 40);
		world = new World();
		renderer = new WorldRenderer(glGraphics, batcher, world);
		fpsCounter = new FPSCounter();
	    startTime = System.nanoTime();
	}

	@Override
	public void update(float deltaTime) {
		if (state == GameState.Ready1) {
	    	updateReady1();
	    }
		if (state == GameState.Ready2) {
			updateReady2();
		}
		if (state == GameState.Ready3) {
			updateReady3();
		}
	    if (state == GameState.Paused) {
	    	updatePaused();
	    }
	    if (state == GameState.Running) {
	    	updateRunning(deltaTime);
	    }
	    if (state == GameState.GameOver) {
	    	updateGameOver();
	    }
	}
	
	private void updateReady1() {
		if (System.nanoTime() - startTime > 2000000000.0f) {
			state = GameState.Ready2;
			startTime = System.nanoTime();
		}
	}
	
	private void updateReady2() {
		if (System.nanoTime() - startTime > 1000000000.0f) {
			state = GameState.Ready3;
			startTime = System.nanoTime();
		}
	}
	
	private void updateReady3() {
		if (System.nanoTime() - startTime > 1000000000.0f) {
	        state = GameState.Running;
	        world.setStartTime(System.nanoTime());
		}
	}
	private void updateRunning(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
	    int len = touchEvents.size();
	    int direction = 0;
	    boolean buttLiftDown = false;
	    for (int i = 0; i < len; i++) {
	        TouchEvent event = touchEvents.get(i);
	        if (event.type != TouchEvent.TOUCH_UP) continue;
	        
	        touchPoint.set(event.x, event.y);
	        cam2d.touchToWorld(touchPoint);
	        
	        if (OverlapTester.pointInRectangle(pauseBounds, touchPoint)) {
//	            Assets.playSound(Assets.clickSound);
	            state = GameState.Paused;
	            return;
	        }            
	        
	        if (OverlapTester.pointInRectangle(upBounds, touchPoint)) {
	        	direction = 1;
	        }
	        
	        if (OverlapTester.pointInRectangle(downBounds, touchPoint)) {
	        	direction = 2;
	        }
	        
	        if (OverlapTester.pointInRectangle(liftBounds, touchPoint)) {
	        	buttLiftDown = true;
	        }
	    }
	    
	    world.update(deltaTime, direction, buttLiftDown);
	    if (world.isGameOver) {
	    	state = GameState.GameOver;
	    	String text;
	    	if (world.player.place == 1) {
	    		text = world.timePastToString() + ". You Won!";
	    	} else {
	    		text = world.timePastToString() + ". You Lose!";
	    	}
	    	overText = Assets.geneText((GLGame)game, text, 32);
	    }
	}
	
	private void updatePaused() {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type != TouchEvent.TOUCH_UP) continue;
			
			touchPoint.set(event.x, event.y);
			cam2d.touchToWorld(touchPoint);
			
			if(OverlapTester.pointInRectangle(resumeBounds, touchPoint)) {
//	            Assets.playSound(Assets.clickSound);
	            state = GameState.Running;
	            return;
	        }
	        
	        if(OverlapTester.pointInRectangle(replay1Bounds, touchPoint)) {
//	            Assets.playSound(Assets.clickSound);
	            game.setScreen(new GameScreen(game));
	            return;
	        }
		}
	}
	
	private void updateGameOver() {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		int len = touchEvents.size();
		for (int i = 0; i < len; i++) {
			TouchEvent event = touchEvents.get(i);
			if (event.type != TouchEvent.TOUCH_UP) continue;
			
			touchPoint.set(event.x, event.y);
			cam2d.touchToWorld(touchPoint);
			
			if(OverlapTester.pointInRectangle(resumeBounds, touchPoint)) {
//	            Assets.playSound(Assets.clickSound);
	            state = GameState.Running;
	            return;
	        }
	        
	        if(OverlapTester.pointInRectangle(replay2Bounds, touchPoint)) {
//	            Assets.playSound(Assets.clickSound);
	            game.setScreen(new GameScreen(game));
	            return;
	        }
		}
	}

	@Override
	public void present(float deltaTime) {
		GL10 gl = glGraphics.getGL();
	    gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	    gl.glEnable(GL10.GL_TEXTURE_2D);
	    
	    renderer.render();
	    
	    cam2d.setViewportAndMatrices();
	    gl.glEnable(GL10.GL_BLEND);
	    gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
	    batcher.beginBatch(Assets.menus);
	    if (state == GameState.Ready1) {
	    	presentReady(0);
	    }
	    if (state == GameState.Ready2) {
	    	presentReady(1);
	    }
	    if (state == GameState.Ready3) {
	    	presentReady(2);
	    }
	    if (state == GameState.Paused) {
	    	presentPaused();
	    }
	    if (state == GameState.Running) {
	    	presentRunning();
	    }
	    if (state == GameState.GameOver) {
	    	presentGameOver();
	    }
	    batcher.endBatch();
	    if (state == GameState.GameOver) {
	    	presentOverText();
	    }
	    
	    if (state == GameState.Running) {
	    	presentStatus();
	    }
	    gl.glDisable(GL10.GL_BLEND);
	    fpsCounter.logFrame();
	}
	
	private void presentReady(int ready) {
		batcher.drawSprite(344+144/2, 136+288/2, 144, 288, Assets.readyMenu[2-ready]);
	}
	
	private void presentRunning() {
		batcher.drawSprite(32, 256, 64, 192, Assets.leftMenu);
		batcher.drawSprite(800-32, 256, 64, 192, Assets.rightMenu);
	}
	
	private void presentStatus() {
		batcher.beginBatch(Assets.speedFontTex);
		Assets.speedFont.drawText(batcher, String.format("%.1f",5*world.player.velocity.x)+" KM /H", 20, 480-24, 18);
		batcher.endBatch();
		batcher.beginBatch(Assets.items);
		batcher.drawSprite(480, 480-22, 640, 44, Assets.minimap);
		presentMapMotors();
		batcher.endBatch();
	}
	
	private void presentMapMotors() {
		float playerMapX = 160 + world.player.position.x/52 * 32;
		float playerMapY = world.player.position.y >= 4.6f ? 480 - 11 : 480 - 33;
		float aiMapX = 160 + world.aiPlayer.position.x/52 * 32;
		float aiMapY = world.aiPlayer.position.y >= 4.6f ? 480 - 11 : 480 - 33;
		batcher.drawSprite(playerMapX, playerMapY, 10, 10, Assets.mapPlayer);
		batcher.drawSprite(aiMapX, aiMapY, 10, 10, Assets.mapAI);
	}
	
	private void presentPaused() {
		batcher.drawSprite(344+144/2, 136+288/2, 144, 288, Assets.pausedMenu);
	}
	
	private void presentGameOver() {
		batcher.drawSprite(344+144/2, 136+288/2, 144, 288, Assets.gameOverMenu);
	}
	
	private void presentOverText() {
		batcher.beginBatch(overText);
		TextureRegion textRegion = new TextureRegion(overText, 0, 0, overText.width, overText.height);
		batcher.drawSprite(344+144/2, 136+288, overText.width, overText.height, textRegion);
		batcher.endBatch();
	}

	@Override
	public void pause() {
		if (state == GameState.Running) state = GameState.Paused;
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}