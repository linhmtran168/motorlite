package com.androidgame.motorlite;

import com.androidgame.framework.gl.GameObject;

public class Obstacle extends GameObject{
	public static final float BRICK_WIDTH = 1.8125f;
	public static final float BRICK_HEIGHT = 0.875f;
	public static final float CAN_WIDTH = 1.375f;
	public static final float CAN_HEIGHT = 1.375f;
	public static final float NAILS_WIDTH = 1.8125f;
	public static final float NAILS_HEIGHT = 0.875f;
		
	public int type;
	private Obstacle(float x, float y, float width, float height) {
		super(x, y, width, height);
	}
	
	static class Brick extends Obstacle {
		public Brick(float x, float y) {
			super(x, y, BRICK_WIDTH, BRICK_HEIGHT);
			type = 0;
		}
	}
	
	static class Can extends Obstacle {
		public Can(float x, float y) {
			super(x, y, CAN_WIDTH, CAN_HEIGHT);
			type = 1;
		}
	}
	
	static class Nails extends Obstacle {
		public final float WIDTH = 1.8125f;
		public final float HEIGHT = 0.875f;
		
		public Nails(float x, float y) {
			super(x, y, NAILS_WIDTH, NAILS_HEIGHT);
			type = 2;
		}
	}
}
