package com.androidgame.motorlite;

import javax.microedition.khronos.opengles.GL10;

//import android.util.Log;

import com.androidgame.framework.gl.Animation;
import com.androidgame.framework.gl.Camera2D;
import com.androidgame.framework.gl.SpriteBatcher;
import com.androidgame.framework.gl.TextureRegion;
import com.androidgame.framework.ngin.GLGraphics;

public class WorldRenderer {
	static final float FRUSTUM_WIDTH = 25;
	static final float FRUSTUM_HEIGHT = 15;
	GLGraphics glGraphics;
	Camera2D cam2d;
	World world;
	SpriteBatcher batcher;
	TextureRegion mapRegion;
	
	public WorldRenderer(GLGraphics glGraphics, SpriteBatcher batcher, World world) {
		this.glGraphics = glGraphics;
		this.batcher = batcher;
		this.world = world;
		this.cam2d = new Camera2D(glGraphics, FRUSTUM_WIDTH, FRUSTUM_HEIGHT);
	}
	
	public void render() {
		cam2d.position.x = world.player.position.x + FRUSTUM_WIDTH/2 - 5;
		cam2d.setViewportAndMatrices();
		renderBackGround();
		renderObstacles();
		renderMotors();
	}
	
	private void renderBackGround() {
		float xRegion = 0;
//		Log.d("Distance", world.player.position.x+"");
		int part = (int)((world.player.position.x - 5)/ 32) % 4;
		xRegion = (world.player.position.x - 5) % 32; 
		batcher.beginBatch(Assets.maps[part]);
		mapRegion = new TextureRegion(Assets.maps[part], xRegion*32, 0, 800, 480);
		batcher.drawSprite(cam2d.position.x, cam2d.position.y, FRUSTUM_WIDTH, FRUSTUM_HEIGHT, mapRegion);
		batcher.endBatch();
		int partNext = part + 1;
		if (part == 3) {
			partNext = 0;
		} 
		batcher.beginBatch(Assets.maps[partNext]);
		mapRegion = new TextureRegion(Assets.maps[partNext], 0, 0, 800, 480);
		batcher.drawSprite(cam2d.position.x + FRUSTUM_WIDTH - xRegion + 6.85f, cam2d.position.y, FRUSTUM_WIDTH, FRUSTUM_HEIGHT, mapRegion);
		batcher.endBatch();
		
		GL10 gl = glGraphics.getGL();
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		batcher.beginBatch(Assets.items);
		batcher.drawSprite(World.WORLD_WIDTH - 26, 6, 13, 13, Assets.flag);
		batcher.endBatch();
		
		int len = world.milestones.size();
		for (int i = 0; i < len; i++) {
			Milestone milestone = world.milestones.get(i);
			batcher.drawSprite(milestone.position.x, milestone.position.y, Milestone.WIDTH, Milestone.HEIGHT, Assets.milestone);
		}
		batcher.endBatch();
		
		batcher.beginBatch(Assets.fontTex);
//		Log.d("font Size", Assets.fontTex.width+"");
		for (int i = 0; i < len; i++) {
			Milestone milestone = world.milestones.get(i);
			float posX = milestone.miles.length() > 1 ? milestone.position.x - 0.2f : milestone.position.x;
			Assets.font.drawText(batcher, milestone.miles, posX, milestone.position.y-0.2f, 32, 0.2f);
		}
		batcher.endBatch();
	}
	
	private void renderMotors() {
		GL10 gl = glGraphics.getGL();
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        batcher.beginBatch(Assets.motors);
        if (world.aiPlayer.position.y < world.player.position.y) {
        	renderAnimatedMotor(world.player);
        	renderAnimatedMotor(world.aiPlayer);
        } else {
        	renderAnimatedMotor(world.aiPlayer);
        	renderAnimatedMotor(world.player);
        }
		batcher.endBatch();
	}
	
	private void renderAnimatedMotor(Motor motor) {
		TextureRegion keyFrame;
		if (motor.motorType == 0) {
			keyFrame = Assets.motor.getKeyFrame(motor.stateTime, Animation.ANIMATION_LOOPING);
		} else {
			keyFrame = Assets.aiMotor.getKeyFrame(motor.stateTime, Animation.ANIMATION_LOOPING);
		}
		if (!motor.isLiftUp) {
			batcher.drawSprite(motor.position.x, motor.position.y, Motor.WIDTH, Motor.HEIGHT, keyFrame);
		} else {
			batcher.drawSprite(motor.position.x, motor.position.y, Motor.WIDTH, Motor.HEIGHT, 45, keyFrame);
		}
	}
	
	private void renderObstacles() {
		GL10 gl = glGraphics.getGL();
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        batcher.beginBatch(Assets.items);
		int len = world.obstacles.size();
//		Log.d("size obstacles", len+"");
		for (int i = 0; i < len; i++) {
			Obstacle obstacle = world.obstacles.get(i);
			switch(obstacle.type) {
			case 0:
				batcher.drawSprite(obstacle.position.x, obstacle.position.y, Obstacle.BRICK_WIDTH, Obstacle.BRICK_HEIGHT, Assets.brick);
				break;
			case 1:
				batcher.drawSprite(obstacle.position.x, obstacle.position.y, Obstacle.CAN_WIDTH, Obstacle.CAN_HEIGHT, Assets.can);
				break;
			case 2:	
				batcher.drawSprite(obstacle.position.x, obstacle.position.y, Obstacle.NAILS_WIDTH, Obstacle.NAILS_HEIGHT, Assets.nails);
				break;
			}
		}
		batcher.endBatch();
	}
}
