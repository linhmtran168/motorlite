package com.androidgame.motorlite;

import com.androidgame.framework.gl.DynamicGameObject;
import com.androidgame.framework.math.Rectangle;

public class Motor extends DynamicGameObject {
	public static final float WIDTH = 4.5f;
	public static final float HEIGHT = 4.5f;
	public static final float MAX_VELOCITY = 30;
	public static final float ACCEL = 4f;
	public boolean isLiftUp;
	public boolean isReachGoal;
	public boolean isStopped;
	public int timePast;
	public float startTime;
	public int place;
	public int motorType;
//	public int state;
	public float stateTime;
	
	public Motor(float x, float y, int type) {
		super(x, y, WIDTH, HEIGHT);
		isLiftUp = false;
		isReachGoal = false;
		isStopped = false;
		timePast = 0;
		startTime = 0;
		stateTime = 0;
//		state = 0;
		this.motorType = type;
		this.bounds = new Rectangle(x - WIDTH/2 + 0.5f, y - HEIGHT/2, WIDTH - 1f, 1f);
	}
	
	public void update(float deltaTime) {
		if (position.x >= World.WORLD_WIDTH - 26) {
			isReachGoal = true;
		}
		
		if (position.x >= World.WORLD_WIDTH - 20) {
			isStopped = true;
			velocity.x = 0;
			velocity.y = 0;
		}
		
		if (!isStopped) {
			if (velocity.x < MAX_VELOCITY) velocity.add(ACCEL*deltaTime, 0);
			position.add(velocity.x*deltaTime, 0);
			if (!isLiftUp) {
				bounds.lowerLeft.set(position.x -WIDTH/2 + 0.5f, position.y - HEIGHT/2);
			} else {
				bounds.lowerLeft.set(position.x, position.y - HEIGHT/2);
			}
			if (System.nanoTime() - startTime >= 1000000000.0f) {
				timePast++;
				startTime = System.nanoTime();
			}
		}
		
//		if (velocity.x == 0) {
//			state = 0;
//			stateTime = 0;
//		}
		
		stateTime += deltaTime;
	}
	
	public void changeLane(int direct) {
		if (direct == 1) {
			if (position.y < 4.6f) {
				position.y = isLiftUp ? 4.6f + 0.4f : 4.6f;
			}
		}
		if (direct == 2) {
			if (position.y >= 4.6f) {
				position.y = isLiftUp ? 2.8f + 0.4f : 2.8f;
			}
		}
	}
	
	public void lift() {
		isLiftUp = !isLiftUp;
		if (isLiftUp == true) {
			position.y += 0.4f;
			bounds.width = bounds.width / 2;
		} else {
			position.y -= 0.4f;
			bounds.width = bounds.width * 2;
		}
	}
	
	public void hit(boolean isHindered) {
		velocity.x = isHindered ? velocity.x / 4 : velocity.x*0.8f;
	}
}
