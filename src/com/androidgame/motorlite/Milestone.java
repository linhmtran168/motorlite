package com.androidgame.motorlite;

import com.androidgame.framework.gl.GameObject;

public class Milestone extends GameObject {
	public static final float WIDTH = 2;
	public static final float HEIGHT = 2;
	
	String miles;
	public Milestone(float x, float y) {
		super(x, y, WIDTH, HEIGHT);
	}
}
