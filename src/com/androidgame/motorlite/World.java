package com.androidgame.motorlite;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.androidgame.framework.math.OverlapTester;

public class World {
	public static final float WORLD_WIDTH = 1040;
	public static final float WORLD_HEIGHT = 15;
	
	public final Motor player;
	public final AIMotor aiPlayer;
	public final List<Obstacle> obstacles;
	public final List<Milestone> milestones;
	public final Random rand;
	
	public boolean isGameOver;
	
	public World() {
		this.player = new Motor(5, 2.8f, 0);
		this.aiPlayer = new AIMotor(5, 4.6f, 1);
		this.player.timePast = 0;
		this.aiPlayer.timePast = 0;
		this.obstacles = new ArrayList<Obstacle>();
		this.milestones = new ArrayList<Milestone>();
		rand = new Random();
		isGameOver = false;
		generateRoad();
	}
	
	public void generateRoad() {
		int x = 25;
		int y = 50;
		while (x < WORLD_WIDTH - 36) {
			addObstacle(x, rand.nextInt(3));
			x += 6+rand.nextInt(20);
		}
		while (y < WORLD_WIDTH - 26) {
			Milestone milestone = new Milestone(y, 5);
			milestone.miles = (20 - y/50) + "";
			milestones.add(milestone);
			y += 50;
		}
	}
	
	private void addObstacle(float x, int type) {
		float y_random = rand.nextInt(2) == 0? 0.8f : 2.8f;
		if (type == 0) {
			Obstacle obstacle = new Obstacle.Brick(x, y_random);
			obstacles.add(obstacle);
		} else if (type == 1) {
			Obstacle obstacle = new Obstacle.Can(x, y_random);
			obstacles.add(obstacle);
		} else {
			Obstacle obstacle = new Obstacle.Nails(x, y_random);
			obstacles.add(obstacle);
		}
	}
	
	public void update(float deltaTime, int direction, boolean buttLiftDown) {
		checkGameOver();
		updateMotor(deltaTime, direction, buttLiftDown);
		checkClash();
	}
	
	private void updateMotor(float deltaTime, int direction, boolean buttLiftDown) {
		if (buttLiftDown) {
			player.lift();
		}
		if (direction != 0) {
			player.changeLane(direction);
		}
		if (!isGameOver) {
			updatePlace();
		}
		aiPlayer.lift();
		aiPlayer.hinder(player);
		player.update(deltaTime);
		aiPlayer.update(deltaTime);
	}
	
	private void updatePlace() {
		if (aiPlayer.position.x < player.position.x) {
			aiPlayer.place = 2;
			player.place = 1;
		} else if (aiPlayer.position.x > player.position.x) {
			player.place = 2;
			aiPlayer.place = 1;
		} else {
			player.place = aiPlayer.place = 1;
		}
	}
	
	private void checkClash() {
		checkClashObstacles();
		checkClashMotors();
	}
	
	private void checkClashObstacles() {
		int len = obstacles.size();
		for (int i = 0; i < len; i++) {
			Obstacle obstacle = obstacles.get(i);
			float dist = obstacle.position.x - player.position.x;
			float dist2 = obstacle.position.x - aiPlayer.position.x;
			if (dist >= -4.5f && dist <= 6) {
				if (OverlapTester.overlapRectangles(player.bounds, obstacle.bounds)) {
					player.hit(true);
				}
			}
			
			if (dist2 >= -4.5f && dist2 <= 6) {
				if (dist2 <= 6 && isSameLane(aiPlayer.position.y, obstacle.position.y)) {
					if (obstacle.position.y == 0.8f) {
						aiPlayer.evade(1);
					} else {
						aiPlayer.evade(2);
					}
				}
				if (OverlapTester.overlapRectangles(aiPlayer.bounds, obstacle.bounds)) {
					aiPlayer.hit(true);
				}
			}
		}
	}
	
	private void checkClashMotors() {
		float gapX = player.position.x - aiPlayer.position.x;
		float gapY = player.position.y - aiPlayer.position.y;
		if (gapX > 0 && gapX <= 6) {
			if (gapY == 0 || gapY == 0.4f || gapY == -0.4f) {
				if (aiPlayer.position.y >= 4.6) {
					aiPlayer.evade(2);
				} else {
					aiPlayer.evade(1);
				}
			}
		
		}
		
		if (gapX >= -4.5f && gapX <= 4.5f) {
			if (OverlapTester.overlapRectangles(player.bounds, aiPlayer.bounds)) {
				if (aiPlayer.position.x != player.position.x) {
//					Motor hinderedOne = aiPlayer.position.x > player.position.x ? player : aiPlayer;
//					hinderedOne.hit(true);
					if (aiPlayer.position.x > player.position.x) {
						player.hit(true);
						aiPlayer.hit(false);
					} else {
						player.hit(false);
						aiPlayer.hit(true);
					}
				} else {
					if (rand.nextInt(2) == 0) {
						player.hit(true);
						aiPlayer.hit(false);
					} else {
						player.hit(false);
						aiPlayer.hit(true);
					}
				}
			}
		}
	}
	
	private void checkGameOver() {
		if (player.isReachGoal) {
			if (player.timePast < aiPlayer.timePast) {
				player.place = 1;
				aiPlayer.place = 2;
			} else {
				player.place = 2;
				aiPlayer.place = 1;
			}
		}
		
		if (player.isStopped) {
			isGameOver = true;
		}
	}
	
	private boolean isSameLane(float motorY, float obstacleY) {
		float gap = motorY - obstacleY;
		if (gap < 1 || gap > 3) {
			return false;
		}
		return true;
	}
	
	public void setStartTime(float time) {
		player.startTime = time;
		aiPlayer.startTime = time;
	}
	
	public String timePastToString() {
		int minute = (int) (player.timePast < 0 ? 0 : player.timePast / 60);
		int second = (int) (player.timePast < 0 ? 0 : (player.timePast % 60));
		String minute_s = "" + minute;
		String second_s = second < 10 ? "0" + second : "" + second;
		String time = minute_s + ":" + second_s;
		return time;
	}
}
